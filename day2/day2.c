
#include <stdio.h>
#include <string.h>

struct Location {
  int height;
  int x;
  int y;
};

void modify_location(struct Location *loc, char *where, int how_much) {
  if (strcmp(where, "Forward") == 0) {
    loc->x += how_much;
    return;
  }

  if (strcmp(where, "Backwards") == 0) {
    loc->x -= how_much;
    return;
  }

  if (strcmp(where, "Left") == 0) {
    loc->y += how_much;
    return;
  }

  if (strcmp(where, "Right") == 0) {
    loc->y -= how_much;
    return;
  }

  if (strcmp(where, "Up") == 0) {
    loc->height -= how_much;
    return;
  }

  if (strcmp(where, "Down") == 0) {
    loc->height += how_much;
    return;
  }
}

int main(int argc, char **argv) {

  struct Location loc;
  loc.height = 0;
  loc.x = 0;
  loc.y = 0;

  while (1) {
    char command[50];
    int number;
    scanf("%s %d", command, &number);
    modify_location(&loc, command, number);
    printf("x: %d\ny: %d\nh: %d\n", loc.x, loc.y, loc.height);
  }
}
