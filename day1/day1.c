#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

struct SonnarResults {
  int8_t first_line;
  int increases;
  int last_measurement;
};

FILE *open_file() {
  FILE *fptr = fopen("input.txt", "r");
  if (fptr == NULL) {
    printf("Cannot find file\n");
    exit(EXIT_FAILURE);
  }
  return fptr;
}

void count_line(struct SonnarResults *results, int line) {
  if (line > results->last_measurement && !results->first_line) {
    results->increases++;
    printf("%d (increase)\n", line);
  } else {
    printf("%d (decrease)\n", line);
  }

  if (results->first_line) {
    results->first_line = 0;
  }

  results->last_measurement = line;
}

int count_increases_in_file(FILE *file) {
  int num;
  struct SonnarResults results;

  // Needed for line reading
  char *line_str;
  size_t line_len;
  char *ptr;
  int read_success;

  results.first_line = 1;

  getline(&line_str, &line_len, file);
  while (feof(file) != 1) {
    num = strtol(line_str, &ptr, 10);
    count_line(&results, num);
    getline(&line_str, &line_len, file);
  }

  free(line_str);
  free(ptr);

  return results.increases;
}

int main(int argc, char **argv) {
  FILE *file = open_file();
  int increases = count_increases_in_file(file);
  printf("Increases: %d\n", increases);
  fclose(file);
  return 0;
}
