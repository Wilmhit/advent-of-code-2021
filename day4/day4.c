#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct Board {
  bool *marked[5];
  int *numbers[5];
};

struct Numbers {
  int *numbers;
  int how_many;
};

struct Game {
  int turn;
  struct Numbers numbers;
  int num_of_boards;
  struct Board boards[10];
};

void print_board(struct Board *board) {
  printf("\n");
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      int padding = 3;
      int number = board->numbers[i][j];
      char *checkmark = board->marked[i][j] ? "x" : "o";

      printf("% *d%s", padding, number, checkmark);
    }
    printf("\n");
  }
}

void print_game(struct Game *game) {
  printf("Turn: %d\n", game->turn);
  for (int i = 0; i < game->num_of_boards; i++) {
    print_board(&game->boards[i]);
  }
}

FILE *open_file() {
  FILE *fptr = fopen("input.txt", "r");
  if (fptr == NULL) {
    printf("Cannot find file\n");
    exit(EXIT_FAILURE);
  }
  return fptr;
}

struct Numbers read_numbers_row(char *string, char *separator) {
  struct Numbers numbers = {.how_many = 0};
  char *tokenized[200];

  const char *tok;
  for (tok = strtok(string, separator); tok && *tok;
       tok = strtok(NULL, separator)) {
    tokenized[numbers.how_many] = strdup(tok);
    numbers.how_many++;
  }

  // remove \n from last line
  char *last = tokenized[numbers.how_many - 1];
  last[strlen(last) - 1] = '\0';

  numbers.numbers = malloc(numbers.how_many * sizeof(int));
  for (int i = 0; i < numbers.how_many; i++) {
    numbers.numbers[i] = (int)strtol(tokenized[i], NULL, 10);
  }

  return numbers;
}

struct Board strings_to_board(char *strings[]) {
  struct Board board = {0};

  for (int i = 0; i < 5; i++) {
    board.numbers[i] = malloc(5 * sizeof(int));
    board.marked[i] = malloc(5 * sizeof(bool));
    for (int j = 0; j < 5; j++) {
      board.marked[i][j] = false;
    }
  }

  for (int i = 0; i < 5; i++) {
    struct Numbers numbers = read_numbers_row(strings[i], " ");
    if (numbers.how_many != 5) {
      printf("Board is in wrong format");
      exit(EXIT_FAILURE);
    }
    board.numbers[i] = numbers.numbers;
  }
  return board;
}

struct Game read_game(FILE *file) {
  /*
    File structure is as follows:
      Winning numbers (comma separated)
      Empty line
      First Board (5 lines, space separated)
      Empty line
      Second Board (5 lines, space separated)
      Empty line
      ...
  */

  struct Game game = {0};
  size_t line_size = 200;
  char *line = malloc(line_size * sizeof(char));

  if (line == NULL) {
    printf("Not enough memory!");
    exit(EXIT_FAILURE);
  }

  getline(&line, &line_size, file);
  game.numbers = read_numbers_row(line, ",");

  game.num_of_boards = 0;
  while (feof(file) == 0) {
    // read empty line
    getline(&line, &line_size, file);
    if (strcmp(line, "\n") != 0 || feof(file) != 0) {
      break;
    }

    // read board
    char *strings[5];
    for (int i = 0; i < 5; i++) {
      getline(&line, &line_size, file);
      strings[i] = strdup(line);
    }
    game.boards[game.num_of_boards] = strings_to_board(strings);
    game.num_of_boards++;
  }

  free(line);
  return game;
}

void simulate_game(struct Game *game) {
  for (game->turn = 0; game->turn != game->numbers.how_many; game->turn++) {
    print_game(game);
    sleep(1);
  }
}

int main(int argc, char *argv[]) {
  FILE *file = open_file();
  struct Game game = read_game(file);
  fclose(file);
  simulate_game(&game);

  return EXIT_SUCCESS;
}
