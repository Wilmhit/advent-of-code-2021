#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define line_len 5
#define DEBUG 1

struct GammaRate {
  int already_counted;
  int ones[line_len];
};

FILE *open_file() {
  FILE *fptr = fopen("input.txt", "r");
  if (fptr == NULL) {
    printf("Cannot find file\n");
    exit(EXIT_FAILURE);
  }
  return fptr;
}

void count_new_input(char *string, struct GammaRate *rate) {
  if (strlen(string) != line_len + 1) {
    printf("String %s is not of length %d! Length: %d", string, line_len + 1,
           (int)strlen(string));
    exit(EXIT_FAILURE);
  }

  rate->already_counted++;
  if (DEBUG) {
    printf("Now scanning %s", string);
  }

  for (int i = 0; i < line_len; i++) {
    if (string[i] == '1') {
      rate->ones[i]++;
    }
  }
}

bool should_be_one(int already_counted, int ones) {
  if (DEBUG) {
    printf("Counted ones: %d\n", ones);
  }

  if (already_counted - ones < already_counted / 2) {
    if (DEBUG) {
      printf("Gave true\n");
    }
    return true;
  }
  printf("Gave false\n");
  return false;
}

int get_gamma(struct GammaRate *rate) {
  bool position[line_len];
  for (int i = 0; i < line_len; i++) {
    position[i] = should_be_one(rate->already_counted, rate->ones[i]);
  }

  int gamma = 0;
  for (int i = 0; i < line_len; i++) {
    gamma = gamma << 1;
    if (position[i]) {
      gamma++;
    }
  }
  return gamma;
}

int invert_4_bits(int number) {
  int xor = 0;
  for (int i = 0; i < line_len; i++) {
    xor = xor << 1;
    xor++;
  }

  return number ^ xor;
}

int main(int argc, char **argv) {
  FILE *file = open_file();

  char *read_string;
  size_t scanned_len;
  struct GammaRate gamma = {0};

  getline(&read_string, &scanned_len, file);
  while (feof(file) != 1) {
    count_new_input(read_string, &gamma);
    getline(&read_string, &scanned_len, file);
  }
  int gamma_num = get_gamma(&gamma);
  int epsilon_num = invert_4_bits(gamma_num);
  int power = gamma_num * epsilon_num;

  printf("Gamma: %d\nEpsilon: %d\nPower: %d\n", gamma_num, epsilon_num, power);
  return EXIT_SUCCESS;
}
